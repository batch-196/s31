
let http = require("http");
http.createServer(function (request, response) {

    if (request.url === "/") {
        response.writeHead(200, { 'Content-Type': 'text/plain' });
        response.end("Hello from our first Server! This is from / endpoint.");
    } else if (request.url === "/profile") {
        response.writeHead(200, { 'Content-Type': 'text/plain' });
        response.end("Hi! I'm <yourName>!");
    }

}).listen(4000);

console.log("Server is running on localHost:4000!");